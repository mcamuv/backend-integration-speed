FROM registry.gitlab.com/docker.public/tomcat:8.5-oracle-jdk1.8

MAINTAINER Oscar Mateo <omateo@americatel.com.pe>

ARG mvn_version

ENV MAVEN_VERSION $mvn_version

WORKDIR /tmp

RUN mkdir app_wars && cd app_wars

RUN curl -L -o backend-integration-speed-${MAVEN_VERSION}.war http://10.16.16.114:8081/nexus/repository/amp-maven-releases/pe/com/americatel/backend-integration-speed/1.0.0.0/backend-integration-speed-${MAVEN_VERSION}.war

RUN cp backend-integration-speed-${MAVEN_VERSION}.war /opt/tomcat/webapps

RUN rm -rf /tmp/app_wars
